#/usr/bin/python

from coinbase.wallet.client import Client
import datetime
import MySQLdb
# import config
import os
import math
import time
import json
# import mysql_driver



# get current path
current_path = os.path.abspath(__file__)
current_path_temp = current_path.split("/")
current_path_temp = current_path_temp[:-1]
current_path = ''
for path_temp in current_path_temp:
	current_path += path_temp + "/"

config = json.load(open(current_path + 'config/config.txt'))

db = MySQLdb.connect(host=config['db_host'],user=config['db_user'],passwd=config['db_password'],db=config['db_database'])
cursor = db.cursor()

client = Client(config['api_key'], config['api_secret'], api_version=config['api_version'])

currency = 'EUR' # can also use EUR, CAD, etc.
values = config['values']


class Mysql_driver():

	def __init__(self, cursor):
		self.cursor = cursor

	def write(self, table, parameters, operation, condition='', condition_value=''):
	    query = ""
	    if operation.upper() == "INSERT":
			cols = ""
			vals = ""
			for param in parameters:
				if cols != "":
					cols += ", "
					vals += ", "
				cols += str(param)
				vals += "'%s'" % str(parameters[param])
			query = "INSERT INTO %s (%s) VALUES (%s)" % (table, cols, vals)
			self.cursor.execute(query)

	    if operation.upper() == "UPDATE":
			query_parameters = ''
			for param in parameters:
				if query_parameters != "":
					query_parameters += ", "
				query_parameters += "%s='%s'" % (str(param), str(parameters[param]))

			query = "UPDATE %s SET %s WHERE %s='%s'" % (table, query_parameters, condition, condition_value)
			self.cursor.execute(query)

	def getlast(self, table, value):
		query = "select * from %s where currency='%s' ORDER BY id DESC LIMIT 1" % (table, value)
		self.cursor.execute(query)

		parameters = {}
		if self.cursor.rowcount > 0:
			field_names = [i[0] for i in self.cursor.description]

			for results in self.cursor:
				ii = 0
				for result in results:
					field_temp = field_names[ii]
					parameters[field_names[ii]] = result
					ii += 1
		else:
		 	parameters['current_budget'] = 0

		return parameters

	def getvalues(self, value):
		parameters = {}
		query = "select * from budget WHERE currency='%s'" % value
		self.cursor.execute(query)

		if self.cursor.rowcount > 0:
			row = self.cursor.fetchone()
			parameters['eur_budget'] = row[0]
			parameters['buy_treshold'] = row[2]
			parameters['sell_treshold'] = row[3]
		else:
			parameters['eur_budget'] = 0
			parameters['buy_treshold'] = 0
			parameters['sell_treshold'] = 0
		return parameters

class Transaction():

	def __init__(self, transaction):
		self.transaction = transaction

	def fee_1(self):
		return math.ceil(self.transaction / float(fee_fix)) + 1

	def fee_2(self):
		return math.ceil(self.transaction * fee_var) / 100

	def fee(self):
		return self.fee_1() + self.fee_2()

	def value(self):
		return self.transaction

	def buyed(self, price_amount):
		conversion_rate = 1 / price_amount
		return (self.transaction - self.fee()) * conversion_rate

	def converted(self, price_amount):
		return self.buyed(price_amount) * price_amount


operation_read = Mysql_driver(cursor)

current_budget = {}
max_pay = 100
fee_fix = 200
fee_var = 2.45

log = ''

for value in values:
	lastvalues = operation_read.getlast("transactions", value)
	parameters = operation_read.getvalues(value)
	budget = parameters['eur_budget']
	price_min = parameters['buy_treshold']
	sell_treshold = parameters['sell_treshold']
	current_budget[value] = lastvalues['current_budget']

	currency_pair = value + '-' + currency

	hour = datetime.datetime.now() + datetime.timedelta(hours=config['hour_difference'])
	price = client.get_spot_price(currency_pair = currency_pair)
	price_amount = float(price.amount)
	# sell_treshold = price_min * (100 + treshold) / 100

	t = Transaction(current_budget[value]*price_amount)
	s = Transaction(current_budget[value]*sell_treshold)

	log += "========================\n"
	log += str(hour) + "\n"
	log += "Current %s value: %s\n" % (value, price_amount)
	log += "My %s budget: %s (%s EUR)" % (value, current_budget[value], current_budget[value] * price_amount)
	if current_budget[value] > 0:
		log += " - will sell at %i" % (s.value() + s.fee())
	log += "\n"
	log += "My EUR budget: %s\n" % (budget)

	s = Transaction(current_budget[value]*sell_treshold)

	action = ""
	converted = 0

	debug = 1
	if (current_budget[value]*price_amount) > (s.value() + s.fee()):

		log += "Current price is over treshold! Will sell everything\n"

		# t = Transaction(current_budget[value]*price_amount)

		log += "Total fee: %s\n" % t.fee()
		sold = t.buyed(price_amount)
		converted = sold * price_amount
		log += "Total sold EUR: %s\n" % (converted)
		log += "Total sold %s: %s\n" % (value, sold)
		budget += converted
		current_budget[value] = 0
		# price_min = price_min * 1.1
		action = "sell"
		operation_net = sold

	if price_amount < price_min:

		if budget >= max_pay:

			log += "Current price is under min limit: will spend %s EUR!\n" % max_pay
			t = Transaction(max_pay)

			log += "Total fee: %s\n" % t.fee()
			buyed = t.buyed(price_amount)
			converted = buyed * price_amount
			log += "Total buyed EUR: %s\n" % (converted)
			log += "Total buyed %s: %s\n" % (value, buyed)
			current_budget[value] += buyed
			budget -= max_pay
			action = "buy"
			operation_net = buyed

	filename = '%sbuylog/%s_log.log' % (current_path, value)

	file = open(filename,'a+')
	file.write(log + '\n')
	file.close()

	if action != "":
		table = "transactions"
		parameters = {}
		parameters['date'] = hour
		parameters['real_currency'] = currency
		parameters['currency'] = value
		parameters['amount'] = price_amount
		parameters['current_budget'] = current_budget[value]
		parameters['current_budget_eur'] = current_budget[value] * price_amount
		parameters['eur_budget'] = budget
		parameters['buy_treshold'] = price_min
		parameters['sell_treshold'] = sell_treshold
		parameters['operation'] = action
		parameters['operation_amount'] = t.value()
		parameters['operation_fee'] = t.fee()
		parameters['operation_net'] = converted
		parameters['debug'] = debug

		operation = Mysql_driver(cursor)
		operation.write(table, parameters, 'insert')

		newvalues = {}
		newvalues['budget'] = budget
		operation.write('budget', newvalues, 'update', 'currency', value)

	db.commit()

print log
db.close()
