#/usr/bin/python

from coinbase.wallet.client import Client
import datetime
import MySQLdb
import json
import os


# get current path
current_path = os.path.abspath(__file__)
current_path_temp = current_path.split("/")
current_path_temp = current_path_temp[:-1]
current_path = ''
for path_temp in current_path_temp:
	current_path += path_temp + "/"

config = json.load(open(current_path + 'config/config.txt'))

db = MySQLdb.connect(host=config['db_host'],user=config['db_user'],passwd=config['db_password'],db=config['db_database'])
cursor = db.cursor()

client = Client(config['api_key'], config['api_secret'], api_version=config['api_version'])

currency = 'EUR' # can also use EUR, CAD, etc.
values = config['values']

hour = datetime.datetime.now() + datetime.timedelta(hours=config['hour_difference'])

for value in values:
	currency_pair = value + '-' + currency

	price = client.get_spot_price(currency_pair = currency_pair)
	text = 'Current %s price in %s: %s' % (values[value], currency, price.amount)

	query = """INSERT INTO prices (date, real_currency, currency, price) VALUES ('%s', '%s', '%s', '%s')""" % (hour, currency, value, price.amount)
	cursor.execute(query)

	filename = '%slogs/%s_log.log' % (current_path, value)

	file = open(filename,'a+')
	file.write(str(hour) + '\n')
	file.write(text + '\n')
	file.close()

db.commit()
db.close()
