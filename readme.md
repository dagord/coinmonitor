This scripts uses Coinbase API to get Bitcoin, Bitcoin cash, Ethereum and Litecoin values in realtime (in EUR).
It simulates buy/sell transactions according to the tresholds set by the user through the web interface

**Initial setup**

Register in Coinbase and get your APIs (https://developers.coinbase.com/).

Install requirements:
`pip install coinbase`
`pip install mysql`

Create a local database and import the coinmonitor.sql dump.

Copy *config.txt.sample* to *config/config.txt* and setup your Coinbase API, your local database parameters and so on.

Launch Apache webserver and access the scripts/index.php from your browser to setup the buy and sell tresholds for the supported currencies.

To get current currencies values:
`python getprice.py`

To simulate buy/sell transactions:
`python buytest.py`

Refresh the browser to see the latest updates; it's better to run the Python scripts through crontab.
