<html>
<head>
    <title>Coinbase</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

    <script language="Javascript">
        function select_currency() {
            value = $("#selected_currency").val();
            $(".display").hide(200);
            $("#display_"+value).show(200);
        }

        function submit_form() {
            $("#setvalues").submit();
        }

        function reset_form() {
            if (confirm('Are you sure?')) {
                $("#submit").val('reset');
                $("#setvalues").submit();
            }
            else {
                return false;
            }
        }
    </script>

    <style>
        #selected_currency {
            background-color: #efefef;
            font-size: 1.8em;
            height: 50px;
        }

        tr.tr_buy {
            background-color: bisque;
        }

        tr.tr_sell {
            background-color: lightblue;
        }

    </style>

</head>

<body>


<?php

$sthing = file_get_contents("../config/config.txt");
$config = json_decode($sthing, thue);

$conn = new mysqli($config['db_host'], $config['db_user'], $config['db_password'], $config['db_database']);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$display = Array();
foreach ($config['values'] as $key => $value):

    $display['value'] = $key;
    $display[$key]['value_description'] = $value;

    $sql = "SELECT * FROM prices WHERE currency='".$key."' ORDER BY date DESC LIMIT 0,1";
    $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $display[$key]['date'] = $row['date'];
                $display[$key]['price'] = $row['price'];
            }
        }

    $sql = "SELECT * FROM transactions WHERE currency='".$key."' ORDER BY date DESC";
    $result2 = $conn->query($sql);

        $trans = Array();
        if ($result2->num_rows > 0) {
            $ii = 0;
            while($row2 = $result2->fetch_assoc()) {
                $trans[$ii] = $row2;
                if (!isset($display[$key]['my_budget']))
                    $display[$key]['my_budget'] = $row2['current_budget'];
                $ii++;
            }
        }
    $display[$key]['transactions'] = $trans;

    $sql = "SELECT SUM(operation_net) AS total_buy FROM transactions WHERE currency='".$key."' AND operation='buy'";
    $resultBUY = $conn->query($sql);
    if ($resultBUY->num_rows > 0) {
        while($rowBUY = $resultBUY->fetch_assoc()) {
            $display[$key]['total_buy'] = $rowBUY['total_buy'];
        }
    }
    else
        $display[$key]['total_buy'] = 0;


    $sql = "SELECT SUM(operation_net) AS total_sell FROM transactions WHERE currency='".$key."' AND operation='sell'";
    $resultSELL = $conn->query($sql);
    if ($resultSELL->num_rows > 0) {
        while($rowSELL = $resultSELL->fetch_assoc()) {
            $display[$key]['total_sell'] = $rowSELL['total_sell'];
        }
    }
    else
        $display[$key]['total_sell'] = 0;

    $sql = "SELECT * FROM budget WHERE currency='".$key."'";
    $result3 = $conn->query($sql);
    if ($result3->num_rows > 0) {
        while($row3 = $result3->fetch_assoc()) {
            $display[$key]['sell_treshold'] = $row3['sell_treshold'];
            $display[$key]['buy_treshold'] = $row3['buy_treshold'];
            $display[$key]['budget'] = $row3['budget'];
            $ii++;
        }
    }
    else {
        $display[$key]['sell_treshold'] = 0;
        $display[$key]['buy_treshold'] = 0;
        $display[$key]['budget'] = 0;
    }

endforeach;

if (isset($_GET['selected_currency']))
    $selected_currency = $_GET['selected_currency'];
else
    $selected_currency = "BTC";

$conn->close();

echo '<div class="container">';

echo '<form id="setvalues" method="POST" action="save.php">';

?>
<select id="selected_currency" name="selected_currency" class="form-control" onChange="select_currency()">
    <?php
         foreach ($config['values'] as $key => $value):
            ?>
            <option value="<?php echo $key; ?>"
                <?php if ($key == $selected_currency): ?>
                     selected
                <?php endif; ?>
            ><?php echo $key ?> - <?php echo $value ?></option>
            <?php
        endforeach;
    ?>
</select>
<br><br>
<?php

foreach ($config['values'] as $key => $value):
    echo '<div id="display_'.$key.'" class="display"';
    if ($key != $selected_currency)
        echo ' style="display: none"';
    echo '>';
    echo "<b>".$key."</b><br>";
    echo "Value at ".$display[$key]['date'].": <b>".$display[$key]['price']."</b> eur<br>";
    if ($display[$key]['my_budget'] > 0)
        echo "My budget: ".$display[$key]['my_budget']." (<b>".(round($display[$key]['my_budget'] * $display[$key]['price'] * 100)/100)."</b> eur)<br>";

    echo '<div class="row"><div class="col-md-3">Buy treshold</div>';
    echo '<div class="col-md-9"><input type="text" name="buy_treshold['.$key.']" class="form-control" value="'.$display[$key]['buy_treshold'].'">';
    echo '</div></div>';
    echo '<div class="row"><div class="col-md-3">Sell treshold</div>';
    echo '<div class="col-md-9"><input type="text" name="sell_treshold['.$key.']" class="form-control" value="'.$display[$key]['sell_treshold'].'">';
    echo '</div></div>';
    echo '<div class="row"><div class="col-md-3">Budget EUR</div>';
    echo '<div class="col-md-9"><input type="text" name="budget['.$key.']" class="form-control" value="'.$display[$key]['budget'].'">';
    echo '</div></div>';

    echo '<button class="btn" onClick="submit_form()">Submit</button> ';
    echo '<button class="btn" onClick="reset_form()">Reset transactions history</button>';

    echo '<br><br>';

    if (count($display[$key]['transactions']) > 0):

        echo '<div class="row">';
        echo '<div class="col-md-2">Total buy</div>';
        echo '<div class="col-md-2">'.$display[$key]['total_buy'].'</div>';
        echo '<div class="col-md-2">Total sell</div>';
        echo '<div class="col-md-2">'.$display[$key]['total_sell'].'</div>';
        echo '<div class="col-md-2">Difference</div>';
        echo '<div class="col-md-2">'.($display[$key]['total_sell'] - $display[$key]['total_buy']).'</div>';
        echo '</div>';

        echo '<table class="table"><thead>';
        echo "<tr><th>Date</th><th>operation</th><th>price</th><th>transaction amount (EUR)</th><th>transaction amount (".$key.")</th></tr>";
        echo "</thead>";
        foreach ($display[$key]['transactions'] as $trans):
            echo "<tr class=\"tr_".$trans['operation']."\">";
            echo "<td>".$trans['date']."<br>";
            echo "<td>".$trans['operation']."<br>";
            echo "<td>".$trans['amount']."<br>";
            echo "<td>".$trans['operation_net']."<br>";
            echo "<td>".$trans['operation_net'] / $trans['amount']."<br>";
            echo "</tr>";
        endforeach;
        echo "</table>";
    endif;
    echo "</div>";
endforeach;

echo '<input type="hidden" id="submit" name="submit" value="ok">';
echo "</form>";
echo "</div>";

?>

</body>
</html>
